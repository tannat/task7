package com.tannat.task7;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.tannat.task7.entity.CungHoangDao;

public class MainActivity extends Activity implements View.OnClickListener {

    public static final String KEY_DETAIL = "KEY_DETAIL";

    private TextView mTvDetailTitle, mTvDetailText;
    private ImageView mImvDetailIcon;

    private String title, content;
    private int iconSrc;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);

        initViews();
        initData();
    }

//    @Override
//    protected void onStart() {
//        super.onStart();
//        initData();
//    }

    private void initData() {
        // default
        if (title == null || content == null || iconSrc == 0) {
            title = getString(R.string.ma_ket_title1);
            content = getString(R.string.ma_ket_text);
            iconSrc = R.drawable.ic_ma_ket;
        }

        mTvDetailTitle.setText(title);
        mTvDetailText.setText(content);
        mImvDetailIcon.setImageDrawable(getDrawable(iconSrc));
    }

    private void initViews() {
        findViewById(R.id.imv_bach_duong).setOnClickListener(this);
        findViewById(R.id.imv_bao_binh).setOnClickListener(this);
        findViewById(R.id.imv_bo_cap).setOnClickListener(this);
        findViewById(R.id.imv_cu_giai).setOnClickListener(this);
        findViewById(R.id.imv_kim_nguu).setOnClickListener(this);
        findViewById(R.id.imv_ma_ket).setOnClickListener(this);
        findViewById(R.id.imv_nhan_ma).setOnClickListener(this);
        findViewById(R.id.imv_song_ngu).setOnClickListener(this);
        findViewById(R.id.imv_song_tu).setOnClickListener(this);
        findViewById(R.id.imv_su_tu).setOnClickListener(this);
        findViewById(R.id.imv_thien_binh).setOnClickListener(this);
        findViewById(R.id.imv_xu_nu).setOnClickListener(this);

        mTvDetailTitle = findViewById(R.id.tv_detail_title);
        mTvDetailText = findViewById(R.id.tv_detail_text);
        mImvDetailIcon = findViewById(R.id.iv_detail_icon);

        findViewById(R.id.btn_xem_them).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        findViewById(R.id.scv_detail).scrollTo(0,0);
        switch (v.getId()) {
            case R.id.imv_bach_duong:
                title = getString(R.string.bach_duong_title1);
                content = getString(R.string.bach_duong_text);
                iconSrc = R.drawable.ic_bach_duong;
                break;
            case R.id.imv_bao_binh:
                title = getString(R.string.bao_binh_title1);
                content = getString(R.string.bao_binh_text);
                iconSrc = R.drawable.ic_bao_binh;
                break;
            case R.id.imv_bo_cap:
                title = getString(R.string.bo_cap_title1);
                content = getString(R.string.bo_cap_text);
                iconSrc = R.drawable.ic_bo_cap;
                break;
            case R.id.imv_cu_giai:
                title = getString(R.string.cu_giai_title1);
                content = getString(R.string.cu_giai_text);
                iconSrc = R.drawable.ic_cu_giai;
                break;
            case R.id.imv_kim_nguu:
                title = getString(R.string.kim_nguu_title1);
                content = getString(R.string.kim_nguu_text);
                iconSrc = R.drawable.ic_kim_nguu;
                break;
            case R.id.imv_ma_ket:
                title = getString(R.string.ma_ket_title1);
                content = getString(R.string.ma_ket_text);
                iconSrc = R.drawable.ic_ma_ket;
                break;
            case R.id.imv_nhan_ma:
                title = getString(R.string.nhan_ma_title1);
                content = getString(R.string.nhan_ma_text);
                iconSrc = R.drawable.ic_nhan_ma;
                break;
            case R.id.imv_song_ngu:
                title = getString(R.string.song_ngu_title1);
                content = getString(R.string.song_ngu_text);
                iconSrc = R.drawable.ic_song_ngu;
                break;
            case R.id.imv_song_tu:
                title = getString(R.string.song_tu_title1);
                content = getString(R.string.song_tu_text);
                iconSrc = R.drawable.ic_song_tu;
                break;
            case R.id.imv_su_tu:
                title = getString(R.string.su_tu_title1);
                content = getString(R.string.su_tu_text);
                iconSrc = R.drawable.ic_su_tu;
                break;
            case R.id.imv_thien_binh:
                title = getString(R.string.thien_binh_title1);
                content = getString(R.string.thien_binh_text);
                iconSrc = R.drawable.ic_thien_binh;
                break;
            case R.id.imv_xu_nu:
                title = getString(R.string.xu_nu_title1);
                content = getString(R.string.xu_nu_text);
                iconSrc = R.drawable.ic_xu_nu;
                break;
            case R.id.btn_xem_them:
                showDetail();
                return;
            default:
                break;
        }

        mTvDetailTitle.setText(title);
        mTvDetailText.setText(content);
        mImvDetailIcon.setImageDrawable(getDrawable(iconSrc));
    }

    private void showDetail() {
        Intent intent = new Intent();
        intent.setClass(this, DetailAct.class);
        CungHoangDao cungHoangDao = new CungHoangDao(title, content, iconSrc);
        intent.putExtra(KEY_DETAIL, cungHoangDao);
        startActivity(intent);
    }
}
