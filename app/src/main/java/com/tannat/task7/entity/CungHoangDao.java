package com.tannat.task7.entity;

import android.graphics.drawable.Drawable;

import java.io.Serializable;

public class CungHoangDao implements Serializable {
    private String title, content;
    private int iconSrc;

    public CungHoangDao(String title, String content, int iconSrc) {
        this.title = title;
        this.content = content;
        this.iconSrc = iconSrc;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getIconSrc() {
        return iconSrc;
    }

    public void setIconSrc(int iconSrc) {
        this.iconSrc = iconSrc;
    }
}
