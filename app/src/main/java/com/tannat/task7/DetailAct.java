package com.tannat.task7;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.tannat.task7.entity.CungHoangDao;

public class DetailAct extends Activity implements View.OnClickListener {

    private TextView mTvDetailTitle, mTvDetailText;
    private ImageView mImvDetailIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_layout);

        initViews();
        initData();
    }

    private void initData() {
        Intent intent = getIntent();
        CungHoangDao cungHoangDao = (CungHoangDao) intent.getSerializableExtra(MainActivity.KEY_DETAIL);
        if(cungHoangDao != null) {
            mTvDetailTitle.setText(cungHoangDao.getTitle());
            mTvDetailText.setText(cungHoangDao.getContent());
            mImvDetailIcon.setImageDrawable(getDrawable(cungHoangDao.getIconSrc()));
        }
    }

    private void initViews() {
        mTvDetailTitle = findViewById(R.id.tv_detail_title_above);
        mTvDetailText = findViewById(R.id.tv_detail_text);
        mImvDetailIcon = findViewById(R.id.iv_detail_icon);
        findViewById(R.id.tv_detail_title).setVisibility(TextView.INVISIBLE);
        findViewById(R.id.iv_detail_icon_bg).setVisibility(TextView.VISIBLE);
        findViewById(R.id.tv_detail_title_above).setVisibility(TextView.VISIBLE);
        findViewById(R.id.btn_xem_them).setVisibility(View.GONE);

        findViewById(R.id.bt_back).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_back:
                finish();
                break;
            default:
                break;
        }
    }
}
